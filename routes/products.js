'use strict';

/* Express & Router */
const express = require('express');
const products =  express.Router();

/* Require middleware */
import { isAuth } from '../lib/middleware'

// Display product info
products.get('/view/:id', isAuth, function(req, res) {
  var id = req.params.id;
  var sqlite3 = require('sqlite3').verbose();
  let db = new sqlite3.Database('database.sqlite');

  db.get("SELECT * FROM products WHERE id = " + id, function(err, row) {
    console.log(row);
    res.render('pages/product-view', {product: row});
  });

  db.close();
});

// Display cart
products.get('/cart/:id', isAuth, function(req, res) {

  var id = req.params.id;
  var sqlite3 = require('sqlite3').verbose();
  var db = new sqlite3.Database('database.sqlite');

  // Query product by id
  db.get("SELECT * FROM products WHERE id = " + id, function(err, row) {
    console.log(row);
    res.render('pages/cart-view', {product: row});
  });

  db.close();
});

module.exports.products = products;
