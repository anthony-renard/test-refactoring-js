'use strict';

/* Express & Router */
const express = require('express');
const index = express.Router();

// Display index page with all products
index.get('/', function(req, res, next) {

  var sqlite3 = require('sqlite3').verbose();
  var db = new sqlite3.Database('database.sqlite');

  // Query all products from the database
  db.all("SELECT * FROM products", function(err, rows) {
      res.render('pages/product-listing', {products: rows});
  });

  db.close();

});

module.exports.index = index;
