"use strict";

// require express
const express = require('express');

// requires libs
import logger       from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser   from 'body-parser';
import http         from 'http';

// initialize app
const app = express();
const server = http.createServer(app);
let port = process.env.PORT || 3001;

// set app uses
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('views'));
app.use(express.static('public'));

// set twig to view engine
app.set("view engine","twig");

// define routes
app.use('/', require('./routes/index').index);
app.use('/products', require('./routes/products').products);

app.close = function() {
  server.close();
}

app.listen(() => {
  server.listen(port, () => {
    console.log("Express server listening on port " + port + " in " + app.settings.env + " mode");
  });
});

export default app;
